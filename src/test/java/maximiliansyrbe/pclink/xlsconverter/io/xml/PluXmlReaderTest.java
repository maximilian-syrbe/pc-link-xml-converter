package maximiliansyrbe.pclink.xlsconverter.io.xml;

import maximiliansyrbe.pclink.xlsconverter.utils.TestData;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class PluXmlReaderTest {

    @Test
    void readFromFile() throws IOException {
        PLU plu = PLUXmlReader.readFromFile(TestData.PLU_XML_IN);
        assertNotNull(plu);
    }
}
