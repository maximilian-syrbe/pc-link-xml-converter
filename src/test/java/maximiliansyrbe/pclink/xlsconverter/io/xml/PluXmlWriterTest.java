package maximiliansyrbe.pclink.xlsconverter.io.xml;

import maximiliansyrbe.pclink.xlsconverter.utils.TestData;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PluXmlWriterTest {

    @AfterEach
    void cleanUp() {
        new File(TestData.PLU_XML_OUT).delete();
    }

    @Test
    void writeToFile() throws IOException {
        PLU plu = PLUXmlReader.readFromFile(TestData.PLU_XML_IN);
        PLUXmlWriter.writeToFile(plu, TestData.PLU_XML_OUT);
        PLU plu2 = PLUXmlReader.readFromFile(TestData.PLU_XML_IN);

        assertEquals(plu.getRecords().get(0).getPluCode(), plu2.getRecords().get(0).getPluCode());
    }
}
