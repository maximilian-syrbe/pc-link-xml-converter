package maximiliansyrbe.pclink.xlsconverter.io.xls;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import maximiliansyrbe.pclink.xlsconverter.utils.TestData;
import maximiliansyrbe.pclink.xlsconverter.io.xml.PLUXmlReader;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import maximiliansyrbe.pclink.xlsconverter.utils.PluValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.validation.ObjectError;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class PluXlsReaderTest {

    @Autowired
    private PluValidator validator;

    @Test
    void readFromFile() throws IOException, BiffException, WriteException {
        PLU pluXml = PLUXmlReader.readFromFile(TestData.PLU_XML_IN);
        PLUXlsWriter.writePlu2Excel(pluXml, TestData.PRICELIST_XLS_OUT);
        PLU pluExel = PLUXlsReader.readFromFile(TestData.PRICELIST_XLS_OUT);

        Optional<List<ObjectError>> validate = this.validator.validate(pluExel);
        assertTrue(validate.isEmpty());
    }
}
