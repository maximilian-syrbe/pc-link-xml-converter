package maximiliansyrbe.pclink.xlsconverter.service.format;

import maximiliansyrbe.pclink.xlsconverter.utils.StringFormatter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringFormatterTest {

    @Test
    void formatItemText() {
        assertEquals("Suppe       ", StringFormatter.formatItemText("Suppe"));
        assertEquals("Cola 0,5l   ", StringFormatter.formatItemText("Cola 0,5l"));

        boolean exceptionThrown = false;

        try {
            StringFormatter.formatItemText("Colamola123456");
        } catch (IllegalArgumentException e) {
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    void formatPluCodeLong() {
        assertEquals("00000000000123", StringFormatter.formatPluCodeLong("123"));
        assertEquals("00000000000001", StringFormatter.formatPluCodeLong("1"));
        assertEquals("00000000000200", StringFormatter.formatPluCodeLong("200"));

        boolean exceptionThrown = false;

        try {
            StringFormatter.formatPluCodeLong("201");
        } catch (IllegalArgumentException e) {
            exceptionThrown = true;
        }

        assertTrue(exceptionThrown);

        exceptionThrown = false;

        try {
            StringFormatter.formatPluCodeLong("0");
        } catch (IllegalArgumentException e) {
            exceptionThrown = true;
        }

        assertTrue(exceptionThrown);
    }

}
