package maximiliansyrbe.pclink.xlsconverter.model;

import maximiliansyrbe.pclink.xlsconverter.io.xml.PLUXmlReader;
import maximiliansyrbe.pclink.xlsconverter.utils.PluValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.validation.ObjectError;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static maximiliansyrbe.pclink.xlsconverter.utils.TestData.PLU_XML_IN;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class PLUModelTest {

    private PLU plu;

    @Autowired
    private PluValidator validator;

    @BeforeEach
    void loadPLU() throws IOException {
        plu = PLUXmlReader.readFromFile(PLU_XML_IN);
    }

    @Test
    void testRecordListNotToSmall() {
        plu.getRecords().remove(0);

        Optional<List<ObjectError>> validate = validator.validate(plu);
        assertFalse(validate.isEmpty());
    }

    @Test
    void testRecordListNotToLong() {
        Record record = plu.getRecords().get(0);
        plu.getRecords().add(record);
        plu.getRecords().add(record);
        plu.getRecords().add(record);
        plu.getRecords().add(record);

        Optional<List<ObjectError>> validate = validator.validate(plu);
        assertFalse(validate.isEmpty());
    }

    @Test
    void testRecordListNotEmpty() {
        plu.getRecords().clear();

        Optional<List<ObjectError>> validate = validator.validate(plu);
        assertFalse(validate.isEmpty());
    }

    @Test
    void testPluCodeFormat() {
        Record record = plu.getRecords().get(0);

        Optional<List<ObjectError>> valRes;

        record.setPluCode("abc");
        valRes = this.validator.validate(plu);
        assertEquals(1, valRes.get().size());

        record.setPluCode("128");
        valRes = this.validator.validate(plu);
        assertEquals(1, valRes.get().size());

        record.setPluCode("000000a0000000");
        valRes = this.validator.validate(plu);
        assertEquals(1, valRes.get().size());

        record.setPluCode("00000000000000");
        valRes = this.validator.validate(plu);
        assertTrue(valRes.isEmpty());

        record.setPluCode("00000000001234");
        valRes = this.validator.validate(plu);
        assertTrue(valRes.isEmpty());

        record.setPluCode("00000000001200");
        valRes = this.validator.validate(plu);
        assertTrue(valRes.isEmpty());
    }

    @Test
    void testTextLen() {
        Optional<List<ObjectError>> valRes;
        Record record = plu.getRecords().get(0);

        record.getField4().setText("");
        valRes = this.validator.validate(plu);
        assertTrue(valRes.isPresent());

        record.getField4().setText("XXX XXX XXX X");
        valRes = this.validator.validate(plu);
        assertTrue(valRes.isPresent());

        record.getField4().setText("XXX XXX XXX ");
        valRes = this.validator.validate(plu);
        assertTrue(valRes.isEmpty());
    }
}
