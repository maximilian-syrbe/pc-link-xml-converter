package maximiliansyrbe.pclink.xlsconverter.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;

public class StringFormatter {

    public static final int ITEM_TEXT_LEN = 12;
    public static final int PLU_CODE_LEN = 14;
    public static final int PLU_CODE_MAX_SIZE = 200;

    public static String formatItemText(@NotNull final String text) {
        String trimmedText = text.trim();

        if (trimmedText.length() > ITEM_TEXT_LEN) {
            throw new IllegalArgumentException(String.format("Text too long (%s): %s", trimmedText.length(), trimmedText));
        }

        return StringUtils.rightPad(trimmedText, ITEM_TEXT_LEN, " ");
    }

    public static String formatPluCodeLong(@NotNull final String shortPluCode) {
        Assert.hasLength(shortPluCode, "Plu code is empty");

        final int pluCode = Integer.parseInt(shortPluCode);

        if (pluCode < 1) {
            throw new IllegalArgumentException(String.format("PLU_CODE (%s) must be > 0)", pluCode));
        }

        if (pluCode > PLU_CODE_MAX_SIZE) {
            throw new IllegalArgumentException(String.format("PLU_CODE too long (%s). Allowed: %s", pluCode, PLU_CODE_MAX_SIZE));
        }

        return StringUtils.leftPad(shortPluCode, PLU_CODE_LEN, "0");
    }

    public static int formatDepartment(@NotNull final String dept) {
        Assert.hasLength(dept, "Department is empty");

        return Integer.parseInt(dept);
    }
}
