package maximiliansyrbe.pclink.xlsconverter.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class PluValidator {

    private final Validator validator;

    public PluValidator(@Qualifier("defaultValidator") Validator validator) {
        Assert.notNull(validator, "validator is null");
        this.validator = validator;
    }

    public Optional<List<ObjectError>> validate(PLU plu) {
        MapBindingResult bindingResult = new MapBindingResult(
                new ObjectMapper().convertValue(plu, HashMap.class), plu.getClass().getName());

        this.validator.validate(plu, bindingResult);
        return bindingResult.hasErrors() ? Optional.of(bindingResult.getAllErrors()) : Optional.empty();
    }
}
