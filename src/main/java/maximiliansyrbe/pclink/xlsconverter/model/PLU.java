package maximiliansyrbe.pclink.xlsconverter.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JacksonXmlRootElement(namespace = "http://www.w3.org/2001/XMLSchema-instance", localName = "FILE")
public class PLU {

    @JacksonXmlProperty(localName = "RECORD")
    @JacksonXmlElementWrapper(useWrapping = false)
    @Valid
    @Size(min = 200, max = 200)
    private List<Record> records;

    @Override
    public String toString()
    {
        return "ClassPojo [RECORD = "+ records +"]";
    }
}
