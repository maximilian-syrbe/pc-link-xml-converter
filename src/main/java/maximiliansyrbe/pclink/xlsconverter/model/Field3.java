package maximiliansyrbe.pclink.xlsconverter.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Field3 {
    @JacksonXmlProperty(localName = "PRICE")
    private String price;

    @Override
    public String toString()
    {
        return "ClassPojo [PRICE = "+ price +"]";
    }
}
