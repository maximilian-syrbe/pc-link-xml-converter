package maximiliansyrbe.pclink.xlsconverter.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Field1 {
    @JacksonXmlProperty(localName = "DEPARTMENT")
    private Integer department;

    @JacksonXmlProperty(localName = "PROGRAM")
    @Valid
    private Program program;

    @Override
    public String toString()
    {
        return "ClassPojo [DEPARTMENT = "+ department +", PROGRAM = "+ program +"]";
    }
}
