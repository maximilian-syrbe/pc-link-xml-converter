package maximiliansyrbe.pclink.xlsconverter.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Record {

    @JacksonXmlProperty(localName = "TYPE")
    @Min(0)
    @Max(0)
    private int type;

    @JacksonXmlProperty(localName = "FIELD1")
    @Valid
    private Field1 field1;

    @JacksonXmlProperty(localName = "FIELD3")
    @Valid
    private Field3 field3;

    @JacksonXmlProperty(localName = "FIELD4")
    @Valid
    private Field4 field4;

    @JacksonXmlProperty(localName = "PLU_CODE", isAttribute = true)
    @Pattern(regexp = "^0{1}[\\d]{13}$")
    private String pluCode;

    @Override
    public String toString()
    {
        return "ClassPojo [FIELD3 = "+ field3 +", FIELD4 = "+ field4 +", TYPE = "+ type +", PLU_CODE = "+ pluCode +", FIELD1 = "+ field1 +"]";
    }
}
