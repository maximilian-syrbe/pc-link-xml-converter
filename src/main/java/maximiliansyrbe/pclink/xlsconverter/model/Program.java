package maximiliansyrbe.pclink.xlsconverter.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Program {
    @JacksonXmlProperty(localName = "ENTRY_TYPE")
    private String entryType;

    @Override
    public String toString()
    {
        return "ClassPojo [ENTRY_TYPE = "+ entryType +"]";
    }
}
