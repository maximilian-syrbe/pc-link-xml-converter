package maximiliansyrbe.pclink.xlsconverter.service;

import jxl.read.biff.BiffException;
import maximiliansyrbe.pclink.xlsconverter.config.AppProperties;
import maximiliansyrbe.pclink.xlsconverter.io.xls.PLUXlsReader;
import maximiliansyrbe.pclink.xlsconverter.io.xml.PLUXmlWriter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import maximiliansyrbe.pclink.xlsconverter.utils.PluValidator;
import org.springframework.stereotype.Service;
import org.springframework.validation.ObjectError;

import javax.validation.ValidationException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PriceListConverterService {

    private final AppProperties properties;
    private final PluValidator validator;

    public void convertPriceList2Xml() throws IOException, BiffException {
        log.info("Start converting of {} to XML", properties.getPriceListPath());

        PLU plu = PLUXlsReader.readFromFile(properties.getPriceListPath());
        Optional<List<ObjectError>> validation = validator.validate(plu);

        if (validation.isPresent()) {
            log.error("{}", validation.get());
            throw new ValidationException("PLU Validation failed. Aborting.");

        }
        PLUXmlWriter.writeToFile(plu, properties.getXmlExportPath());
    }
}
