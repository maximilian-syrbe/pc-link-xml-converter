package maximiliansyrbe.pclink.xlsconverter.io.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import org.apache.logging.log4j.util.Strings;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class PLUXmlWriter {

    public static void writeToFile(final PLU plu, final String filename) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        String xml = xmlMapper.writeValueAsString(plu);
        final String finalXml = xml
                .replace("xmlns=\"\" ", Strings.EMPTY)
                .replace("FILE xmlns=", "FILE xmlns:xsi=");
        Files.writeString(new File(filename).toPath(), finalXml);
    }
}
