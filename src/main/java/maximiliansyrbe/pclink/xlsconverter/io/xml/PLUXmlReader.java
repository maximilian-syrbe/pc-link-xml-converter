package maximiliansyrbe.pclink.xlsconverter.io.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import maximiliansyrbe.pclink.xlsconverter.model.PLU;
import org.springframework.core.io.ClassPathResource;

import java.io.*;

public class PLUXmlReader {

    public static PLU readFromFile(final String filename) throws IOException {
        File file = new File(filename);
        if (!file.exists()) {
            file = new ClassPathResource(filename).getFile();
        }
        XmlMapper xmlMapper = new XmlMapper();
        String xml = inputStreamToString(new FileInputStream(file));
        PLU value = xmlMapper.readValue(xml, PLU.class);
        return value;
    }

    private static String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
