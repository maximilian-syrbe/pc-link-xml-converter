package maximiliansyrbe.pclink.xlsconverter.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("app")
@Getter
@Setter
public class AppProperties {
    private String priceListPath;
    private String xmlExportPath;
}
